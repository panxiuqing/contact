$(document).ready(function(){
	//new group type
	$("#newgroup").live('click', function() {
		var label = $('<label id="newlabel" class="checkbox inline F"><input type="checkbox" value="result" name="gtid"><input id="newgt" class="checksmall" type="text" name="gtname"></label>');
		$("#newgroup").before(label);
		$("#newgt").blur(function() {
			var value = $(".checksmall").attr('value');
			if (value)
			{
				$(".checksmall").replaceWith(value);
				$.post("/add_gt", {gtname: value}, function(result) {
					$("[value=result]").val(result.gtid);
				});
			}
			else
			{
				$("#newlabel").remove();
			}
		})
	})

	//change gtname
	$(".ctnm").live('click', function() {
		var ctid = $(this).attr("id");
		var str = [$(this).html(), '<span class="caret"></span>'].join('');
		var parent = $(this).parents('div');
		parent.children('button').html(str);
		parent.children('input').attr({name:ctid});
	})

	//create new input area
	$(".ctvalue").live('keydown', function() {
		var parent = $(this).parents("#cts");
		if (parent.next("div").attr("class") != "control-group")
		{
			parent.after(parent.clone());
		}
	})

	//remove next input area if current and next are null
	//remove current input area if current is null and next is nut null
	$(".ctvalue").live('blur', function() {
		var parent = $(this).parents("#cts");
		if ($(this).val() == '' & parent.next("div").attr('id') == "cts")
		{
			if (parent.next("div").find("input").val() == '')
			{
				parent.next("div").remove();
			}
			else
			{
				parent.remove();
			}
		}
	})

	//check name if is provided
	$("#name").live('blur', function() {
		var parent = $(this).parents(".control-group");
		if ($(this).val() == '')
		{
			if (parent.attr('class') != "control-group error") {
				parent.addClass("error");
				parent.children("div").append('<span class="help-inline">MUST</span>');
			}
		}
		else
		{
			$('.linkname').html($(this).val());
			if (parent.attr('class') == "control-group error") {
				parent.toggleClass("error");
				parent.find("span").remove();
			}
		}
	});

	//create new contact type
	$("#newct").live('click', function() {
		var parent = $(this).parents("div.btn-group");
		var button = parent.children("button");
		parent.children("button").replaceWith('<input type="text" id="newctname" class="span1"></input>');
		parent.removeClass("open");
	});

	var button = $("#ctype");
	$("#newctname").live('blur', function() {
		var parent = $(this).parent();
		var value = $(this).val();
		$(this).replaceWith(button);
		if (value != '')
		{
			parent.children("button").html(value + '<span class="caret"></span>');
			$.post("/add_ct", {ctname:value}, function(result) {
				parent.find("li.divider").before('<li><a class="ctnm F" id=' + result.ctid + ' >' + value + '</a></li>');
			})
		}
	})

	//submit edit POST
	//submit add POST
	$("#submit").live('click', function() {
		if ($("#name").val() == '')
		{
			var parent = $("#name").parents(".control-group");
			if (parent.attr('class') != "control-group error") {
				parent.addClass("error");
				parent.children("div").append('<span class="help-inline">MUST</span>');
			}
			$("#name").focus();
		}
		else
		{
			$("#name").parents(".control-group").toggleClass("error");
			$("#name").parents(".control-group").find("span").remove();

			var id = $(".linkname").attr('id');
			var Pic = $("#Pic").attr('name');
			var name = $("#name").val();
			var nick = $("#nick").val();
			var birth = $("#birth").val();
			var homeadr = $("#homeadr").val();
			var remark = $("#remark").val();
			var ctlist = [];
			$(".ctvalue").each(function() {
				if (this.value != '')
				{
					ctlist.push('[' + this.name + ',"' + this.value + '"]');
				}
			});
			var gtlist = [];
			$(":checkbox").each(function() {
				if (this.checked)
				{
					gtlist.push(this.value);
				}
			});

			var post_data = {Pic:Pic, name:name, nick:nick, birth:birth, homeadr:homeadr, remark:remark, ctlist:ctlist, gtlist:gtlist}
			if (id)
			{
				post_data['id'] = id;
				$.post("/edit_linkman", post_data, function(result) {
					location.href = result;
				})
			}
			else
			{
				$.post("/add_linkman", post_data, function(result) {
					location.href = result;
				});
			}
		}
	});
});

	//upload pic		

function UploadPic() {
	var fileObj = $("input#Pic")[0].files[0];
	if (typeof FileReader != 'undefined') {
		var acceptedtypes = {
			'image/png': true,
			'image/jpeg': true,
			'image/gif': true
		};

		if (acceptedtypes[fileObj.type] === true) {
			var reader = new FileReader();
			reader.onload = function (event) {
				var image = new Image();
				image.src = event.target.result;
				image.width = 96;
				image.height = 96;
				$("img").replaceWith(image);
			};
			reader.readAsDataURL(fileObj);
		}
	}
	var form = new FormData();
	form.append('Pic', fileObj);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/upload_img', true);

	xhr.onload = function() {
		if (xhr.status === 200) {
			resJSON = $.parseJSON(xhr.responseText);
			$('#Pic').attr({name:resJSON.filename});

			//console.log('success');
		}
		else
		{
			console.log('error');
		}
	};
	xhr.send(form);
};

function UploadBak() {
	fileObj = $("input#Bak")[0].files[0];
	var form = new FormData();
	form.append('bak', fileObj);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/import_list', true);

	xhr.onload = function(e) {
		if (xhr.status === 200) {
			location.href=e;
		}
		else
		{
			console.log('error');
		}
	};
	xhr.send(form);
}