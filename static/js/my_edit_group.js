$(document).ready(function() {
	$(".group").hover(
		function() {$(this).find("i").toggleClass('hide');},
		function() {$(this).find("i").toggleClass('hide');}
	);
	$(".icon-trash").live('click', function() {
		$.post('/edit_group', {gtid:$(this).attr('id'), operate:'delete'});
		$(this).parents('.group').remove();
	})
	$(".icon-edit").live('click', function() {
		var prev = $(this).parent().prev();
		var value = prev.html();
		prev.replaceWith('<td><input type="text" id="newgtname" value="' + value + '"></td>');
		$(this).parent().prev().children().focus();
	})
	$("#newgtname").live('blur', function() {
		var value=$(this).val();
		var gtid=$(this).parent().next().children().attr('id');
		$(this).replaceWith(value);
		$.post('/edit_group', {gtid:gtid, gtname:value, operate:'update'});
	});
})