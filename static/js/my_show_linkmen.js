$(document).ready(function() {
	$(".lkman").hover(
		function() {$(this).find("i").toggleClass('hide');},
		function() {$(this).find("i").toggleClass('hide');}
	);

	$(".icon-trash").live('click', function() {
		$.post('/delete_linkman', {id:$(this).attr('id')});
		$(this).parents('.lkman').remove();
	})
})

function UploadBak() {
	fileObj = $("input#Bak")[0].files[0];
	var form = new FormData();
	form.append('bak', fileObj);

	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/import_list', true);

	xhr.onload = function(e) {
		if (xhr.status === 200) {
			location.href=e;
		}
		else
		{
			console.log('error');
		}
	};
	xhr.send(form);
}