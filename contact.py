#-*- coding: utf-8 -*-

import sqlite3, sqlparse
from flask import Flask, request, session, g, redirect, url_for,\
    abort, render_template, flash, jsonify, send_from_directory
from contextlib import closing
import hashlib, os
from flaskext.mysql import MySQL

#configuration
#DATABASE = '/tmp/Adr_Bk.db' #sqlite
DEBUG = True
SECRET_KEY = 'development key'
USERNAME = 'admin'
PASSWORD = 'default'
DATABASE_USER = 'jackpan'
DATABASE_PASSWORD = '900412'
DATABASE_DB = 'Adr_Bk'
UPLOAD_FOLDER = 'static/img'
DOWNLOAD_FOLDER = 'static/download'
ALLOWED_EXTENSIONS = set(['png', 'jpeg', 'gif'])

#application
app = Flask(__name__)
app.config.from_object(__name__)
#app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MYSQL_DATABASE_USER'] = DATABASE_USER
app.config['MYSQL_DATABASE_PASSWORD'] = DATABASE_PASSWORD
app.config['MYSQL_DATABASE_DB'] = DATABASE_DB

mysql = MySQL()
mysql.init_app(app)


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

def calcMD5(f):
    md5obj = hashlib.md5()
    md5obj.update(f.read())
    f.seek(0)
    hash = md5obj.hexdigest()
    return hash

#sqlite
#def connect_db():
    #return sqlite3.connect(app.config['DATABASE'])
# def init_db():
#     with closing(sqlite.connect_db()) as db:
#         with app.open_resource('Adr_Bk.sql') as f:
#             db.cursor().executescript(f.read())
#         db.commit()
#         with app.open_resource('import.sql') as f:
#             db.cursor().executescript(f.read())
#         db.commit()

#initial MYSQL databse
def init_db():
    with closing(mysql.connect()) as db:
        with app.open_resource('Adr_Bk.sql') as f:
            db.cursor().execute(f.read())
        db.commit()
        with app.open_resource('import.sql') as f:
            db.cursor().execute(f.read())
        db.commit()

#insert function
def insert(table, fields=(), values=()):
    query = 'insert into %s (%s) values (%s)' % (
        table,
        ', '.join(fields),
        ', '.join(['%s'] * len(values))
        )
    g.cur.execute(query, values)
    g.db.commit()
    id = g.cur.lastrowid
    #g.cur.close()
    return id

def update_g():
    g.cur.execute('select count(*) from Linkman')
    g.num_of_linkman = g.cur.fetchone()[0];

    g.cur.execute('select * from linkmen_v')
    g.linkmen = [dict(id=r[0], name=r[1], email=r[2], emnum=r[3], phone=r[4], phnum=r[5]) for r in g.cur.fetchall()]

    g.cur.execute('select * from linkmen_group_v')
    gts = {}
    for r in g.cur.fetchall():
        if gts.has_key(r[0]):
            gts[r[0]].append(r[1])
        else:
            gts[r[0]] = [r[1]]
    g.gts_of_linkman = gts

    g.cur.execute("select * from group_v")
    g.side_group = [dict(gtid=row[0], gtname=row[1], pnum=row[2]) for row in g.cur.fetchall()]

def update_ct_gt():
    g.cur.execute('select * from CT')
    g.ctypes = [dict(ctid=row[0], ctname=row[1]) for row in g.cur.fetchall()]

    g.cur.execute('select * from GT')
    g.gtypes = [dict(gtid=row[0], gtname=row[1]) for row in g.cur.fetchall()]

def init_g():
    g.base_name = [(u'姓名', 'name', 'text'),
                    (u'昵称', 'nick', 'text'),
                    (u'生日', 'birth', 'date'),
                    (u'家庭地址', 'homeadr', 'text'),
                    (u'公司', 'comp', 'text')]
    g.base_info = {'Pic':'NoPicture.gif'}
    g.linkman_cts = [(1, '', 'phone')]
    g.linkman_gs = []

    update_ct_gt()

@app.before_request
def before_request():
    g.db = mysql.connect()
    g.cur = g.db.cursor()
    init_g()
    update_g()
    #g.db = mysql.connect()

@app.teardown_request
def teardown_request(exception):
    g.db.close()

@app.route('/')
def hello():
    return render_template('login.html', error=None)

@app.route('/show_linkmen')
def show_linkmen():
    return render_template('show_linkmen.html',
                            allnum=g.num_of_linkman,
                            items=g.linkmen,
                            gtnames=g.gts_of_linkman,
                            groups=g.side_group)

@app.route('/show_linkmen/<int:gtid>')
def group_members(gtid):
    _gtid = gtid
    
    #该群组对应的联系人
    g.cur.execute('select * from linkmen_v natural join LG where gtid=%s', _gtid)
    items = [dict(id=row[0], name=row[1], email=row[2], emnum=row[3], phone=row[4], phnum=row[5]) for row in g.cur.fetchall()];

    return render_template('show_linkmen.html',
                            allnum=g.num_of_linkman,
                            items=items,
                            gtnames=g.gts_of_linkman,
                            groups=g.side_group)

@app.route('/add_linkman', methods=['GET', 'POST'])
def add_linkman():
    if not session.get('logged_in'):
        abort(401)

    #GET方法时,显示页面
    if request.method == 'GET':
        return render_template('add_linkman.html',
                                base=g.base_info,
                                lcts=g.linkman_cts,
                                lgs=g.linkman_gs,
                                names=g.base_name,
                                cts=g.ctypes,
                                gts=g.gtypes,
                                groups=g.side_group,
                                allnum=g.num_of_linkman,
                                error=None
                                )

    #使用POST方法时，提交数据
    rf = request.form
    lid = insert('Linkman',
                 ('Pic', 'name', 'nick', 'birth', 'remark', 'homeadr',),
                 (rf['Pic'], rf['name'], rf['nick'], rf['birth'], rf['remark'], rf['homeadr'],))

    print rf['name']

    ctlist = rf.getlist('ctlist[]')     #multdict method
    gtlist = rf.getlist('gtlist[]')

    for ct in ctlist:
        ct = eval(ct)
        insert('LCT',
                ('id', 'ctid', 'value',),
                (lid, ct[0], ct[1],))

    for gtid in gtlist:
        insert('LG',
                ('id', 'gtid',),
                (lid, eval(gtid),))
    
    update_g()
    return url_for('show_linkmen')

@app.route('/delete_linkman', methods=['POST'])
def delete_linkman():
    id = request.form.get('id');
    print id
    #g.cur.execute('call delete_linkman(%s)', id)
    g.cur.execute('delete from Linkman where id=%s', id)
    g.cur.execute('delete from LCT where id=%s', id)
    g.cur.execute('delete from LG where id=%s', id)
    g.db.commit()
    update_g()
    return ''

@app.route('/edit_linkman/<int:id>')
def edit_linkman(id):
    _id = id
    if not session.get('logged_in'):
        abort(401)
    if request.method == 'GET':

        g.cur.execute('select * from Linkman where id=%s', _id)
        Lm = g.cur.fetchone()
        base = dict(lmid=Lm[0], Pic=Lm[1], name=Lm[2], nick=Lm[3], birth=Lm[4], remark=Lm[5], homeadr=Lm[6], comp=Lm[7])

        g.cur.execute('select * from LCT natural join CT where id=%s', _id)
        lcts = [(row[1], row[2], row[3]) for row in g.cur.fetchall()]
        lcts.append((1, '', 'phone'))
        
        g.cur.execute('select * from LG where id=%s', _id)
        lgs = [row[1] for row in g.cur.fetchall()]

        flag = 'edit'
        return render_template('add_linkman.html',
                                lcts=lcts,
                                lgs=lgs,
                                names=g.base_name,
                                base=base,
                                allnum=g.num_of_linkman,
                                cts=g.ctypes,
                                gts=g.gtypes,
                                groups=g.side_group,
                                flag=flag)

@app.route('/edit_linkman', methods=['POST'])
def update_linkman():
    rf = request.form
    _id = rf.get('id')
    fm = ['Pic', 'name', 'nick', 'birth', 'homeadr', 'remark']
    for f in fm:
        g.cur.execute('update Linkman set ' + f + '=%s where id=%s', (rf.get(f), _id))


    g.cur.execute('delete from LCT where id=%s', _id)
    g.cur.execute('delete from LG where id=%s', _id)
    g.db.commit()
    ctlist = rf.getlist('ctlist[]')     #multdict method
    gtlist = rf.getlist('gtlist[]')

    for ct in ctlist:
        ct = eval(ct)
        insert('LCT',
                ('id', 'ctid', 'value',),
                (_id, ct[0], ct[1],))

    for gtid in gtlist:
        insert('LG',
                ('id', 'gtid',),
                (_id, eval(gtid),))

    update_g()
    return url_for('show_linkmen')


#增加group type    
@app.route('/add_gt', methods=['POST'])
def add_gt():
    gtnm = request.form['gtname']
    id = insert('GT', ('gtname',), (gtnm,))
    update_ct_gt()
    return jsonify(gtid=id)

#add contact type
@app.route('/add_ct', methods=['POST'])
def add_ct():
    ctnm = request.form.get('ctname')
    id = insert('CT', ('ctname',), (ctnm,))
    update_ct_gt()
    return jsonify(ctid=id)

@app.route('/edit_group', methods=['GET', 'POST'])
def edit_group():
    if request.method == 'GET':
        g.cur.execute('select * from GT order by gtid')
        egps = [dict(gtid=row[0], gtname=row[1]) for row in g.cur.fetchall()]

        return render_template('edit_group.html',
                                allnum=g.num_of_linkman,
                                egps=egps,
                                groups=g.side_group)
    
    gtid = request.form.get('gtid')
    if request.form.get('operate') == 'delete':
        g.cur.execute('delete from GT where gtid=%s', gtid)
        g.cur.execute('delete from LG where gtid=%s', gtid)
        g.db.commit()
        update_ct_gt()
        update_g()
    else:            
        gtname = request.form.get('gtname')
        g.cur.execute('update GT set gtname=%s where gtid=%s', (gtname, gtid))
        g.db.commit()
        update_ct_gt()
    return ''

@app.route('/import_list', methods=['POST'])
def import_list():
    file = request.files['bak']
    #filename = 'bak.sql'
    #file.save(filename)
    sql = file.read()
    g.cur.execute(sql)
    # sql_parts = sqlparse.split(sql)
    # for sql_part in sql_parts:
    #     if sql_part.strip() == '':
    #         continue
    #     g.cur.execute(sql_part)
    g.db.commit()
    return ""

@app.route('/export_list', methods=['GET', 'POST'])
def export_list():
    os.system('mysqldump Adr_Bk -p900412 > static/download/linkmen.sql')
    return send_from_directory(app.config['DOWNLOAD_FOLDER'], 'linkmen.sql', as_attachment=True)

@app.route('/search', methods=['POST'])
def search():
    name = request.form.get('name')

    #所有联系人信息
    g.cur.execute('select * from linkmen_v where name like %s', '%' + name + '%')
    items = [dict(id=row[0], name=row[1], email=row[2], emnum=row[3], phone=row[4], phnum=row[5]) for row in g.cur.fetchall()];

    return render_template('show_linkmen.html',
                            allnum=g.num_of_linkman,
                            items=items,
                            groups=g.side_group,
                            gtnames=g.gts_of_linkman)

@app.route('/upload_img', methods=['POST'])
def upload_img():
    file = request.files['Pic']
    if file and allowed_file(file.filename):
        filename = calcMD5(file) + '.' + file.filename.rsplit('.',1)[-1]
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return jsonify(filename=filename)
    return jsonify(filename=None)



@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['username'] != app.config['USERNAME']:
            error = 'Invalid username'
        elif request.form['password'] != app.config['PASSWORD']:
            error = 'Invalid password'
        else:
            session['logged_in'] = True
            flash('You were logged in')
            return redirect(url_for('show_linkmen'))
    return render_template('login.html', error=error)

@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out')
    return redirect(url_for('login'))

if __name__ == '__main__':
    app.run('0.0.0.0', 5000)