drop table if exists Linkman;
drop table if exists LC;
drop table if exists Company;
drop table if exists LCT;
drop table if exists CT;
drop table if exists LW;
drop table if exists WT;
drop table if exists LG;
drop table if exists GT;

drop view if exists linkmen_v;
drop view if exists group_v;
/*drop view if exists linkman_v;*/

create table Linkman                    /*Linkman table*/
(id integer primary key auto_increment,	/*linkman id*/
Pic varchar(50),	        /*linkman picture*/
name varchar(10) not null,	        /*linkman name*/
nick varchar(10),		        /*linkman nickname*/
birth date,			        /*linkman birthday*/
remark varchar(50),		        /*remark*/
homeadr varchar(50),
comp varchar(20),
index (name));		        /*linkman's home address*/

create table LCT	/*Linkman and contact type table*/
(id integer,		/*linkman id*/
ctid integer,		/*contact type id*/
value varchar(30));	/*value of contact type*/

create table CT		/*contact type table*/
(ctid integer primary key auto_increment,	/*contact type*/
ctname varchar(20));	/*contact type name*/

create table LW		/*Linkman and Website type table*/
(id integer,		/*linkman id*/
wtid integer,		/*website type id*/
value varchar(30));	/*value of website type*/

create table WT         /*Website type table*/
(wtid integer primary key auto_increment,  /*website type id*/
wtname varchar(20));     /*website type name*/

create table LG         /*Linkman and Group table*/
(id integer,                /*linkman id*/
gtid integer);               /*group type id*/

create table GT         /*group type table*/
(gtid integer primary key auto_increment,              /*group type id*/
gtname varchar(10));      /*group type*/

drop view if exists linkmen_group_v;
create view linkmen_group_v
as
select id, gtname
from LG natural join GT;

drop view if exists linkmen_phone_v;
create view linkmen_phone_v
as
select id, value phone, count(*)-1 phnum
from LCT natural join CT
where ctname='phone'
group by id;

drop view if exists linkmen_email_v;
create view linkmen_email_v
as
select id, value email, count(*)-1 emnum
from LCT natural join CT
where ctname='email'
group by id;

create view linkmen_v
as
select Linkman.id, name, email, emnum, phone, phnum
from Linkman left join linkmen_phone_v on Linkman.id=linkmen_phone_v.id
     left join linkmen_email_v on Linkman.id=linkmen_email_v.id
order by convert(name using gbk) collate gbk_chinese_ci asc;

create view group_v
as
select GT.gtid, gtname, count(*) pnum
from LG natural join GT
group by gtid;