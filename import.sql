insert into CT (ctname) values ('phone');
insert into CT (ctname) values ('email');
insert into CT (ctname) values ('QQ');
insert into CT (ctname) values ('MSN');

insert into GT (gtname) values ('朋友');
insert into GT (gtname) values ('同学');
insert into GT (gtname) values ('家人');
insert into GT (gtname) values ('室友');

insert into Linkman (name) values ('潘秀清');
insert into Linkman (name) values ('王坤');
insert into Linkman (name) values ('莫常新');
insert into Linkman (name) values ('沈宇涛');
insert into Linkman (name) values ('孙燿烽');
insert into Linkman (name) values ('李其雄');

insert into LCT (id, ctid, value) values (1, 1, '13166218817');
insert into LCT (id, ctid, value) values (1, 1, '15216779472');
insert into LCT (id, ctid, value) values (2, 1, '18801954527');
insert into LCT (id, ctid, value) values (3, 1, '18801958237');
insert into LCT (id, ctid, value) values (1, 2, 'panxiuqing@gmail.com');
insert into LCT (id, ctid, value) values (2, 2, 'wangkun@126.com');
insert into LCT (id, ctid, value) values (4, 1, '13166218820');
insert into LCT (id, ctid, value) values (5, 1, '13166218819');
insert into LCT (id, ctid, value) values (6, 1, '13166218825');
insert into LCT (id, ctid, value) values (6, 2, 'lqxpeter@126.com');

insert into LG (id, gtid) values (1, 1);
insert into LG (id, gtid) values (2, 1);
insert into LG (id, gtid) values (2, 2);
insert into LG (id, gtid) values (3, 1);
insert into LG (id, gtid) values (3, 2);
insert into LG (id, gtid) values (4, 1);
insert into LG (id, gtid) values (4, 2);
insert into LG (id, gtid) values (4, 4);
insert into LG (id, gtid) values (5, 1);
insert into LG (id, gtid) values (5, 2);
insert into LG (id, gtid) values (5, 4);
insert into LG (id, gtid) values (6, 1);
insert into LG (id, gtid) values (6, 2);
insert into LG (id, gtid) values (6, 4);
